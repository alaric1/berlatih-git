<?php
require "Animal.php";
require "Frog.php";
require "Ape.php";

$domba = new Animal ("shaun");
echo "Nama : ".$domba->name."<br>";
echo "Legs : ".$domba->legs."<br>";
echo "cold blooded : ".$domba->cold_blooded."<br><br>";

$kodok = new Frog ("buduk");
echo "Nama : ".$kodok->name."<br>";
echo "Legs : ".$kodok->legs."<br>";
echo "cold blooded : ".$kodok->cold_blooded."<br>";
echo "Jump : ".$kodok->jump()."<br><br>";

$sungokong = new Ape ("kera sakti");
echo "Nama : ".$sungokong->name."<br>";
echo "Legs : ".$sungokong->legs."<br>";
echo "cold blooded : ".$sungokong->cold_blooded."<br>";
echo "Yell : ".$sungokong->yell()."<br><br>";

// function ubah_huruf($string){
//     //kode di sini
//     $len=strlen($string);
//     for($i=0; $i<=$len; $i++){
//         $ambil=substr($string,$i,$i);
//         echo ++$ambil.'<br>';
//     }
//     }
    
//     // TEST CASES
//     echo ubah_huruf('wow'); // xpx
//     echo ubah_huruf('developer'); // efwfmpqfs
//     echo ubah_huruf('laravel'); // mbsbwfm
//     echo ubah_huruf('keren'); // lfsfo
//     echo ubah_huruf('semangat'); // tfnbohbu

function tukar_besar_kecil($string){
    //kode di sini
    }
    
    // TEST CASES
    echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
    echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

// $kalimat = "Saya sedang belajar PHP";
//   $katayangdicari = "PHP";
//   $posisi = strpos($kalimat, $katayangdicari);
//   echo $posisi;

?>