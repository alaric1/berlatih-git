@extends('laravel2.layout.main')

@section('judul')
Detail Cast {{ $detail->id }}
@endsection

@section('isi')
<h1>Nama: {{ $detail->name }}</h1>
<h3>Umur: {{ $detail->umur }}</h3>
<p>Bio: {{ $detail->bio }}</p>
<a href="/cast" class="btn btn-secondary">Kembali</a>
@endsection