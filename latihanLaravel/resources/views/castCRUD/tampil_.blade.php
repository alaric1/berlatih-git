@extends('laravel2.layout.main')

@section('judul')
List Cast
@endsection

@section('isi')
<div style="margin-bottom: 20px">
    <a href="/cast/create" class="btn btn-secondary">Tambah Cast</a>
</div>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($tampil as $key => $item)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->umur }}</td>
                <td>{{ $item->bio }}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete"></input>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="5">
                    <center>Silahkan tambahkan cast</center> 
                </td>
            </tr>
        @endforelse
        
    </tbody>
</table>
@endsection