@extends('laravel2.layout.main')

@section('judul')
Tambah Cast
@endsection

@section('isi')
<div class="card-body">
    <form action="/cast" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label >Nama</label>
                <input type="text" class="form-control" name="name" placeholder="Masukkan Nama">
            </div>
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
                <label>Umur</label>
                <input type="number" class="form-control" name="umur" placeholder="Masukkan Umur">
            </div>
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
                <label>Bio</label>
                <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>
            </div>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        {{-- <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        </div> --}}
    </form>
@endsection