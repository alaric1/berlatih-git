@extends('laravel2.layout.main')

@section('judul')
Edit Cast {{ $edit_->name }}
@endsection

@section('isi')
<div class="card-body">
    <form action="/cast/{{ $edit_->id }}" method="POST">
        @csrf
        @method('put')
        <div class="card-body">
            <div class="form-group">
                <label >Nama</label>
                <input type="text" value="{{ $edit_->name }}" class="form-control" name="name" placeholder="Masukkan Nama">
            </div>
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
                <label>Umur</label>
                <input type="number" value="{{ $edit_->umur }}" class="form-control" name="umur" placeholder="Masukkan Umur">
            </div>
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
                <label>Bio</label>
                <textarea name="bio" cols="30" rows="10" class="form-control">{{ $edit_->bio }}</textarea>
            </div>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>
    </form>
@endsection