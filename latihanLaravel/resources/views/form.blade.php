<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="POST">
        @csrf
        <label for="namaDepan">First Name:</label><br><br>
        <input type="text" name="namaDepan"><br><br>

        <label for="namaLast">Last Name:</label><br><br>
        <input type="text" name="namaLast"><br><br>

        <label for="jk">Gender:</label><br><br>
        <input type="radio" name="jk1">Male <br>
        <input type="radio" name="jk1">Female <br>
        <input type="radio" name="jk1">Other <br><br>

        <label for="nationality">Nationality</label><br><br>
        <select name="nationality">
            <option value="indonesia">Indonesia 
            <option value="arabsaudi">Arab Saudi 
            <option value="palestina">Palestina 
            <option value="amerika">Amerika 
        </select><br><br>

        <label for="bahasa">Language Spoken</label><br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br><br>

        <label for="bio">Bio</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>

        <input type="submit">
    </form>

</body>
</html>