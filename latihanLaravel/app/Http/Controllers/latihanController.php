<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class latihanController extends Controller
{
    public function form(){
        return view('form');
    }

    public function akhir(Request $request){
        $first_ = $request['namaDepan'];
        $last = $request['namaLast'];
        return view('welcome_', compact('first_', 'last'));
    }

    public function awal(){
        return view('index');
    }
}
