<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view ('castCRUD.awal');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required'],
        [
            'name.required' => 'Namanya diisi yaa',
            'umur.required'  => 'Eits umurnya juga yaa',
            'bio.required'  => 'Bio jgn dibiarin kosong lah hehe']);
        $query = DB::table('casts')->insert([
            "name" => $request["name"],
            "umur" => $request["umur"],
            "bio" => $request["bio"],
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $tampil = DB::table('casts')->get();
        return view('castCRUD.tampil_', compact('tampil'));
    }

    public function show($id)
    {
        $detail = DB::table('casts')->where('id', $id)->first();
        // dd($detail);
        return view('castCRUD.showData', compact('detail'));
    }

    public function edit($id)
    {
        $edit_ = DB::table('casts')->where('id', $id)->first();
        return view('castCRUD.editData', compact('edit_'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required'],
        [
            'name.required' => 'Namanya diisi yaa',
            'umur.required'  => 'Eits umurnya juga yaa',
            'bio.required'  => 'Bio jgn dibiarin kosong lah hehe']);
        $query = DB::table('casts')
        -> where('id', $id)
        -> update([
            "name" => $request["name"],
            "umur" => $request["umur"],
            "bio" => $request["bio"],
        ]);
        return redirect('/cast');
    }

    
    public function destroy($id)
    {
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
