<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::post('welcome', 'latihanController@akhir');
Route::get('register', 'latihanController@form');
// Route::get('/', 'latihanController@awal');
Route::get('/', function(){
    return view('welcome_');
});
Route::get('table', function(){
    return view('laravel2.tables.table');
});
Route::get('data-table', function(){
    return view('laravel2.tables.dataTable');
});

Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');